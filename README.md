projectdoc Project Management Model
===================================

## Overview

This is a free add-on for [projectdoc](https://www.smartics.eu/confluence/display/PDAC1/) for Confluence.

The add-on provides the blueprints to create pages for

  * Alternatives
  * Decisions
  * Minutes
  * Open Issues
  * Release Notes

It also provides space blueprints to get started with your documentation project quickly.

## Fork me!
Feel free to fork this project to adjust the templates according to your project requirements.

The projectdoc Project Management Doctypes Add-on is licensed under [Apache License Version 2.0](http://www.apache.org/licenses/LICENSE-2.0)

## Documentation

For more information please visit

  * the [add-on's homepage](https://www.smartics.eu/confluence/display/PDAC1/Doctypes+for+Project+Management)
  * Coming soon: the [add-on on the Atlassian Marketplace](https://marketplace.atlassian.com/plugins/de.smartics.atlassian.confluence.smartics-projectdoc-confluence-space-prjmgmt)
