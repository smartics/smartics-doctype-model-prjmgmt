/*
 * Copyright 2014-2018 smartics, Kronseder & Reiner GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.smartics.projectdoc.prjmgmt;

import de.smartics.projectdoc.atlassian.confluence.admin.space.SpaceAdminCenter;
import de.smartics.projectdoc.atlassian.confluence.util.ProjectdocUser;

import com.atlassian.confluence.pages.PageManager;
import com.atlassian.confluence.plugins.createcontent.api.contextproviders.BlueprintContext;
import com.atlassian.confluence.user.AuthenticatedUserThreadLocal;
import com.atlassian.user.User;

/**
 * Provides information for a release note.
 */
public class ProjectdocReleaseNoteContextProvider
    extends AbstractProjectDocContextProvider {
  // ********************************* Fields *********************************

  // --- constants ------------------------------------------------------------

  // --- members --------------------------------------------------------------

  private final ProjectdocUser user;

  /**
   * Provides access to resolve page identifiers.
   */
  private final PageManager pageManager;

  /**
   * Provides access to space properties and services.
   */
  private final SpaceAdminCenter spaceAdminCenter;

  // ****************************** Initializer *******************************

  // ****************************** Constructors ******************************

  public ProjectdocReleaseNoteContextProvider(final ProjectdocUser user,
      final PageManager pageManager, final SpaceAdminCenter spaceAdminCenter) {
    this.user = user;
    this.pageManager = pageManager;
    this.spaceAdminCenter = spaceAdminCenter;
  }

  // ****************************** Inner Classes *****************************

  // ********************************* Methods ********************************

  // --- init -----------------------------------------------------------------

  // --- get&set --------------------------------------------------------------

  // --- business -------------------------------------------------------------

  @Override
  protected BlueprintContext updateBlueprintContext(
      final BlueprintContext blueprintContext) {
    final User user = AuthenticatedUserThreadLocal.get();
    if (user != null) {
      final TemplateContextPopulator populator = new TemplateContextPopulator(
          blueprintContext, this.user, spaceAdminCenter, pageManager);

      populator.populate();

      addSpaceKeyElementContext(blueprintContext);
    }

    updateContextFinally(blueprintContext);

    return blueprintContext;
  }


  // --- object basics --------------------------------------------------------

}
