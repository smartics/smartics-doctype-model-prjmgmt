require(['ajs', 'confluence/root', 'de/smartics/projectdoc/modules/core'], function (AJS, Confluence, PROJECTDOC) {
  "use strict";

  AJS.bind("blueprint.wizard-register.ready", function () {
    Confluence.Blueprint.setWizard(
      'de.smartics.atlassian.confluence.smartics-projectdoc-confluence-space-prjmgmt:create-doctype-template-release-note',
      function (wizard) {
        wizard.on('pre-render.page1Id', function (e, state) {
          state.wizardData.selectedWebItemData = PROJECTDOC.prerenderWizard(e, state);
        });

        wizard.on('submit.page1Id', function (e, state) {
          const version = state.pageData["projectdoc_doctype_release-note_version"];
          if (!version) {
            alert('Please provide a version for this release.');
            return false;
          }

          const shortDescription = state.pageData["projectdoc_doctype_common_shortDescription"];
          if (!shortDescription) {
            alert('Please provide a short description for the release.');
            return false;
          }

          PROJECTDOC.adjustToLocation(state);
        });

        wizard.on('post-render.page1Id', function (e, state) {
          const title = state.wizardData.title;
          if (title) {
            AJS.$('#projectdoc_doctype_release-note_version').val(title);
            AJS.$('#projectdoc_doctype_common_shortDescription').focus();
          }
        });

        wizard.assembleSubmissionObject = PROJECTDOC.assemblePageCreationData;
      }
    );
  });
});