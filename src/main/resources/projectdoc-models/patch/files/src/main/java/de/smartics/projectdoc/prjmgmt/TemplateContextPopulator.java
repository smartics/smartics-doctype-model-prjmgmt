/*
 * Copyright 2014-2018 smartics, Kronseder & Reiner GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.smartics.projectdoc.prjmgmt;

import de.smartics.projectdoc.atlassian.confluence.admin.space.SpaceAdminCenter;
import de.smartics.projectdoc.atlassian.confluence.admin.space.SpaceAdminConsole;
import de.smartics.projectdoc.atlassian.confluence.admin.space.SpaceConfigConstants;
import de.smartics.projectdoc.atlassian.confluence.api.doctypes.Doctype;
import de.smartics.projectdoc.atlassian.confluence.tools.parser.ConfluenceDateParser;
import de.smartics.projectdoc.atlassian.confluence.tools.parser.VersionParser;
import de.smartics.projectdoc.atlassian.confluence.tools.projects.ProjectConstants;
import de.smartics.projectdoc.atlassian.confluence.util.ProjectdocUser;

import com.atlassian.confluence.pages.PageManager;
import com.atlassian.confluence.plugins.createcontent.api.contextproviders.BlueprintContext;
import com.atlassian.confluence.util.i18n.I18NBean;

import org.apache.commons.lang3.StringUtils;

import java.util.Date;

/**
 * Populates the context for the release notes template with information to be
 * added to the created document.
 */
class TemplateContextPopulator {
  // ********************************* Fields *********************************

  // --- constants ------------------------------------------------------------

  static final String VERSION = "projectdoc_doctype_release-note_version";

  static final String KEY_VERSION_EXPANDED =
      "projectdoc_doctype_release-note_version_expanded";

  // --- members --------------------------------------------------------------

  private final BlueprintContext blueprintContext;

  private final ProjectdocUser user;

  private final I18NBean i18n;

  private final SpaceAdminConsole console;

  /**
   * Provides access to resolve page identifiers.
   */
  private final PageManager pageManager;

  // ****************************** Initializer *******************************

  // ****************************** Constructors ******************************

  /**
   * Default constructor.
   *
   */
  TemplateContextPopulator(final BlueprintContext blueprintContext,
      final ProjectdocUser user, final SpaceAdminCenter spaceAdminCenter,
      final PageManager pageManager) {
    this.blueprintContext = blueprintContext;
    this.user = user;
    this.i18n = user.createI18n();
    final String spaceKey = blueprintContext.getSpaceKey();
    this.console = spaceAdminCenter.createConsoleFor(spaceKey);
    this.pageManager = pageManager;
  }

  // ****************************** Inner Classes *****************************

  // ********************************* Methods ********************************

  // --- init -----------------------------------------------------------------

  // --- get&set --------------------------------------------------------------

  // --- business -------------------------------------------------------------

  void populate() {
    final String productName = calcProductName();
    blueprintContext.put(SpaceConfigConstants.PROJECT_NAME, productName);

    addJiraQueryParameterValues(productName);
    addCreationDate();

    final String version = String.valueOf(blueprintContext.get(VERSION));

    expandVersion(version);

    final String name = adjustTitle(productName, version);
    blueprintContext.put("projectdoc_doctype_common_name", name);
  }

  private void expandVersion(final String version) {
    final VersionParser parser = new VersionParser();
    final String expandedVersion = parser.expandVersion(version);
    blueprintContext.put(KEY_VERSION_EXPANDED, expandedVersion);
  }

  private void addCreationDate() {
    final ConfluenceDateParser parser =
        new ConfluenceDateParser(i18n, user.getDateFormatter());
    final Date date = new Date();
    final String datePicker = parser.createDatePicker(date);
    blueprintContext.put("projectdoc_doctype_common_creationDateXml",
        datePicker);

    blueprintContext.put("projectdoc_doctype_common_creationDateMs",
        String.valueOf(date.getTime()));
  }

  private void addJiraQueryParameterValues(final String projectName) {
    final String jiraProjectName = calcJiraProjectName(projectName);
    blueprintContext.put("projectdoc_jira_project", jiraProjectName);

    final String jiraName =
        console.getPropertyAsString(SpaceConfigConstants.JIRA_NAME);
    if (jiraName != null) {
      blueprintContext.put("projectdoc_jira_name", jiraName);
    }
  }

  private String calcProductName() {
    String projectName =
        console.getPropertyAsString(SpaceConfigConstants.PROJECT_NAME);
    if (StringUtils.isBlank(projectName)) {
      projectName = console.getPropertyAsString(ProjectConstants.NAME);
    }
    if (StringUtils.isBlank(projectName)) {
      projectName =
          console.getPropertyAsString(SpaceConfigConstants.PROJECT_ARTIFACT_ID);
    }
    return projectName;
  }

  private String calcJiraProjectName(final String projectName) {
    String jiraProjectName =
        console.getPropertyAsString(SpaceConfigConstants.JIRA_PROJECT);

    if (StringUtils.isBlank(jiraProjectName)
        && StringUtils.isNotBlank(projectName)) {
      return projectName;
    }

    if (StringUtils.isBlank(jiraProjectName)) {
      jiraProjectName = "PROJECT_NAME";
    }

    return jiraProjectName;
  }

  private String adjustTitle(final String productName, final String version) {
    final String documentName;
    if (StringUtils.isNotBlank(productName)) {
      documentName = i18n.getText("projectdoc.doctype.release-notes.label",
          new Object[] {version, productName});
    } else {
      documentName = i18n.getText("projectdoc.doctype.release-notes.for",
          new Object[] {version});
    }

    final String title = calcUniqueTitle(documentName);
    blueprintContext.setTitle(title);
    return documentName;
  }

  private String calcUniqueTitle(final String original) {
    int counter = 1;
    String current = original;
    while (pageManager.getPage(blueprintContext.getSpaceKey(),
        current) != null) {
      current = original + " #" + counter;
      counter++;
    }
    return current;
  }

  // --- object basics --------------------------------------------------------

}
