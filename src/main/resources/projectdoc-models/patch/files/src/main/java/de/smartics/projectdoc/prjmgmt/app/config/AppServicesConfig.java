/*
 * Copyright 2013-2025 Kronseder & Reiner GmbH, smartics
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.smartics.projectdoc.prjmgmt.app.config;

import com.atlassian.confluence.security.SpacePermissionManager;
import com.atlassian.confluence.user.UserAccessor;
import com.atlassian.confluence.pages.PageManager;

import de.smartics.projectdoc.atlassian.confluence.admin.space.SpaceAdminCenter;
import de.smartics.projectdoc.atlassian.confluence.blueprint.provider.ContextProviderSupportService;
import de.smartics.projectdoc.atlassian.confluence.util.ProjectdocUser;

import de.smartics.projectdoc.prjmgmt.ProjectdocReleaseNoteContextProvider;
import de.smartics.projectdoc.prjmgmt.subspace.PartitionContextProvider;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.Bean;

@Configuration
@Import({ConfluenceComponentImports.class, ProjectdocComponentImports.class, AppConfluenceComponentImports.class})
public class AppServicesConfig {
  @Bean
  public PartitionContextProvider partitionContextProvider(
      final ContextProviderSupportService support) {
    return new PartitionContextProvider(support);
  }

  @Bean
  public ProjectdocReleaseNoteContextProvider projectdocReleaseNoteContextProvider(
      final ProjectdocUser user, final PageManager pageManager,
      final SpaceAdminCenter spaceAdminCenter) {
    return new ProjectdocReleaseNoteContextProvider(user, pageManager, spaceAdminCenter);
  }
}